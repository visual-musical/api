const NOTES = require("./note.js");

module.exports = function(freq) {
    switch (freq) {
        case NOTES.DO0:
            return 'rgb(42, 42, 213)';
        case NOTES.SOL0:
            return'rgb(42, 42, 42)';
        case NOTES.LAd0:
            return 'rgb(42, 42, 128)';
        case NOTES.DO1:
            return 'rgb(213, 213, 213)';
        case NOTES.DO2:
            return 'rgb(213, 213, 128)';
        case NOTES.DOd1:
            return 'rgb(213, 128, 213)';
        case NOTES.FA0:
            return 'rgb(213, 128, 128)';
        case NOTES.FA1:
            return 'rgb(213, 128, 42)';
        case NOTES.FA2:
            return 'rgb(213, 42, 213)';
        case NOTES.FAd1:
            return 'rgb(213, 42, 128)';
        case NOTES.FAd2:
            return 'rgb(213, 42, 42)';
        case NOTES.LA1:
            return 'rgb(128, 213, 213)';
        case NOTES.LA2:
            return 'rgb(128, 213, 128)';
        case NOTES.LAd1:
            return 'rgb(128, 213, 42)';
        case NOTES.LAd2:
            return 'rgb(128, 128, 213)';
        case NOTES.MI1:
            return 'rgb(128, 128, 128)';
        case NOTES.MI2:
            return 'rgb(128, 128, 42)';
        case NOTES.RE1:
            return 'rgb(128, 42, 213)';
        case NOTES.RE2:
            return 'rgb(128, 42, 128)';
        case NOTES.REd1:
            return 'rgb(128, 42, 42)';
        case NOTES.REd2:
            return 'rgb(42, 213, 213)';
        case NOTES.SI1:
            return 'rgb(42, 213, 128)';
        case NOTES.SI2:
            return 'rgb(42, 213, 42)';
        case NOTES.SOL1:
            return 'rgb(42, 128, 213)';
        case NOTES.SOL2:
            return 'rgb(42, 128, 128)';
        case NOTES.SOLd1:
            return 'rgb(42, 128, 42)';
        case NOTES.SOLd2:
            return 'rgb(213, 213, 42)';
    }
}
