const NOTES = require("./note.js");

module.exports = function (r, g, b) {
    const maxColor = 255;
    const interval = 85;
    if (r >= maxColor - interval) {
        if (g >= maxColor - interval) {
            if (b >= maxColor - interval) {
                return NOTES.DO1
            } else if (b >= maxColor - interval * 2) {
                return NOTES.DO2
            } else {
                return NOTES.SOLd2
            }
        } else if (g >= maxColor - interval * 2) {
            if (b >= maxColor - interval) {
                return NOTES.DOd1
            } else if (b >= maxColor - interval * 2) {
                return NOTES.FA0
            } else {
                return NOTES.FA1
            }
        } else {
            if (b >= maxColor - interval) {
                return NOTES.FA2
            } else if (b >= maxColor - interval * 2) {
                return NOTES.FAd1
            } else {
                return NOTES.FAd2
            }
        }
    } else if (r >= maxColor - interval * 2) {
        if (g >= maxColor - interval) {
            if (b >= maxColor - interval) {
                return NOTES.LA1
            } else if (b >= maxColor - interval * 2) {
                return NOTES.LA2
            } else {
                return NOTES.LAd1
            }
        } else if (g >= maxColor - interval * 2) {
            if (b >= maxColor - interval) {
                return NOTES.LAd2
            } else if (b >= maxColor - interval * 2) {
                return NOTES.MI1
            } else {
                return NOTES.MI2
            }
        } else {
            if (b >= maxColor - interval) {
                return NOTES.RE1
            } else if (b >= maxColor - interval * 2) {
                return NOTES.RE2
            } else {
                return NOTES.REd1
            }
        }
    } else {
        if (g >= maxColor - interval) {
            if (b >= maxColor - interval) {
                return NOTES.REd2
            } else if (b >= maxColor - interval * 2) {
                return NOTES.SI1
            } else {
                return NOTES.SI2
            }
        } else if (g >= maxColor - interval * 2) {
            if (b >= maxColor - interval) {
                return NOTES.SOL1
            } else if (b >= maxColor - interval * 2) {
                return NOTES.SOL2
            } else {
                return NOTES.SOLd1
            }
        } else {
            if (b >= maxColor - interval) {
                return NOTES.DO0
            } else if (b >= maxColor - interval * 2) {
                return NOTES.LAd0
            } else {
                return NOTES.SOL0
            }
        }
    }
}
