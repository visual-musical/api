const express = require('express');
const app = express();
const colorToNote = require("./colorToNote.js");
const noteToColor = require("./noteToColor.js");
const port = 3000;
const NOTES = require('./note.js');
const http = require('http');
const melodies = require('./melodies');
const hash = require('./hash');

const server = http.createServer(app);

const WebSocket = require('ws');

const wss = new WebSocket.Server({ server: server });

const allWS = {};

wss.on('connection', ws => {
    const ip = ws._socket.remoteAddress;
    ws.isAlive = true;
    ws.on('message', message => {
        if (message === 'pong') {
            return ws.isAlive = true;
        }
        allWS[ip] = { ws, id: hash(ip).toString() };
        console.log(ip, allWS[ip].id);
    });
});

setInterval(() => {
    wss.clients.forEach(ws => {
        if (ws.isAlive === false) {
            delete allWS[ws._socket.remoteAddress];
            return ws.terminate();
        }
        ws.isAlive = false;
        ws.send('ping');
    });
}, 2000);

app.all('*', (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Expose-Headers', 'location');
    next();
});

app.get('/devices/:id/color', (req, res) => {
    const { r, g, b, duration } = req.query;
    const { id } = req.params;

    const freq = colorToNote(r, g, b);
    console.log(freq);
    console.log(Object.keys(NOTES).find(key => NOTES[key] === freq));

    const obj = Object.values(allWS).find(obj => obj.id === id);

    if (obj === undefined) {
        return res.status(404).send('Not found');
    }

    obj.ws.send(`${freq}-${duration}`);

    return res.status(200).send();
});

app.get('/devices/list', (req, res) => {
    res.send(Object.values(allWS).map(({ id }) => id));
});

app.get('/melodies/:kind', (req, res) => {
    const kind = req.params.kind;
    if (melodies[kind] === undefined) {
        return res.status(404).send('Not found');
    }
    res.send(
        melodies[kind].reduce(
            (acc, i) =>
                acc += `<div style="width: 100%; height: ${i[1] * 500}px; background-color: ${noteToColor(i[0])}"></div>`,
            ''
        )
        + `<script>setTimeout(() => setInterval(() => window.scrollBy(0,1), 2), 5000);</script>`
    )
});

server.listen(port, () => console.log(`Example app listening on port ${port}!`));
