const NOTES = require('../note');

const gamme = [
    [NOTES.DO1, 1],
    [NOTES.RE1, 1],
    [NOTES.MI1, 1],
    [NOTES.FA1, 1],
    [NOTES.SOL1, 1],
    [NOTES.LA1, 1],
    [NOTES.SI1, 1],
];

module.exports = {
    gamme
};
