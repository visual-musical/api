const easy = require('./easy');
const got = require('./got');

module.exports = {
    ...easy,
    ...got,
};
